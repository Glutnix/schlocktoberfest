<?php
	
$emailHeaders = [
	'from'    => 'Schlocktoberfest <mailgun@' . $domain . '>',
    'to'      => '<brett@webfroot.co.nz>',
    'subject' => 'Movie Suggestion: ' . $title
];

?>



Hi Schlockpickers,

A kind suggestion that we show "<?= $title ?>" at Schlocktoberfest was just submitted by <?= $email ?>.

<?php if ($newsletter): ?>
They also signed up to the newsletter!
<?php endif; ?>

Thanks again,

Your benevolent overlord website robots.